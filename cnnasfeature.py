'''
Treat a good CNN output on the dogs as features to a SVM
'''
from keras.applications.inception_v3 import InceptionV3, preprocess_input
from keras.preprocessing import image
from multiprocessing import Process
from matplotlib import pyplot as plt
from sklearn.svm import SVC
from sklearn.metrics import f1_score
from sklearn.ensemble import BaggingClassifier
from sklearn.externals import joblib
from tqdm import tqdm
import time
import numpy as np
from sys import argv
from datareader import DataReader, FINISHED

ATON = False

if ATON:
    print("WARNING, RUNNING IN ATON MODE")
    dataReader = DataReader("/home/diedre/bigdata/atondata2") # aton
else:
    dataReader = DataReader("/home/diedre/bigdata/dogs")
FEATURE_SIZE = 2048 


def loadclassifier(featurename, bagging=False, ctest=1, kernel='rbf'):
    if bagging is True:
        bagg = "bagg"
    else:
        bagg = ""
    with open(featurename+kernel+str(ctest)+"-svm-"+bagg+".pkl", 'rb') as f:
        classifier = joblib.load(f)
    print(classifier)
    return classifier

def printscore(classifier, features, labels, c, mode):
    print("C=" + str(c) + str(mode)+" accuracy: " + str(classifier.score(features, labels)))
    
    if mode != "train":
        predictions = classifier.predict(features)
        f1 = f1_score(labels, predictions, average='macro')
        print("C=" + str(c) + str(mode) + "F1=" + str(f1))


def validate(featurename, classifier=None, c=1):
    if classifier is None:
        classifier = loadclassifier(featurename)
    print("C=" + str(c) + " loading features")
    trainfeatures = np.load("train"+featurename + ".npy")
    tlabels = trainfeatures[:,FEATURE_SIZE].astype(int)
    tfeatures = trainfeatures[:,0:FEATURE_SIZE]
    print("Train features: " + str(tfeatures.shape))
    
    valfeatures = np.load("val"+featurename + ".npy")
    vlabels = valfeatures[:,FEATURE_SIZE].astype(int)
    vfeatures = valfeatures[:,0:FEATURE_SIZE]
    print("Val features: " + str(vfeatures.shape))

    testfeatures = np.load("testfeaturesiv3" + ".npy")
    telabels = testfeatures[:,FEATURE_SIZE].astype(int)
    tefeatures = testfeatures[:,0:FEATURE_SIZE]
    print("Test features: " + str(tefeatures.shape))

    print("C=" + str(c) + "Computing accuracies...")
    trainjob = Process(target=printscore, args=(classifier, tfeatures, tlabels, c, "train"))
    valjob = Process(target=printscore, args=(classifier, vfeatures, vlabels, c, "val"))
    testjob = Process(target=printscore, args=(classifier, tefeatures, telabels, c, "test"))
    trainjob.start()
    valjob.start()
    testjob.start()
    
    trainjob.join()
    valjob.join()
    testjob.join()

    print("Accuracies finished")


def trainclassifier(featurename, bagging=False, ctest=1, kernel='rbf'):
    features = np.load("train"+featurename + ".npy")
    labels = features[:,FEATURE_SIZE].astype(int)
    features = features[:,0:FEATURE_SIZE]
    print(labels.shape)
    print(features.shape)
    svm = SVC(C=ctest, kernel=kernel)
    if bagging is True:
        print("Training bag of svms classifier...")
        classifier = BaggingClassifier(base_estimator=svm, n_jobs=-2, verbose=1).fit(features, labels)
        bagg = "bagg"
    else:
        print("Training svm classifier C=" + str(ctest))
        classifier = svm.fit(features, labels)
        bagg = ""
    with open(featurename+kernel+str(ctest)+"-svm-"+bagg+".pkl", 'wb') as f:
        joblib.dump(classifier, f)
    validate(featurename, classifier, c=ctest)


def generateFeatures(name, mode, aug=False):
    print("Generating features in " + mode + " mode.")
    dataReader.changeMode(mode)
    
    print("Loading InceptionV3...")
    feature_extractor = InceptionV3(input_shape=(299,299,3))
    feature_extractor.layers.pop()
    feature_extractor.outputs = [feature_extractor.layers[-1].output]
    feature_extractor.layers[-1].outbound_nodes = []
    #feature_extractor.summary()
    print("Loading done.")
    
    print("Generating features...")

    nimages = dataReader.read_counts[mode][1]
    if aug is False:
        features = np.zeros((nimages, FEATURE_SIZE+1), dtype=np.float64)
    else:
        features = np.zeros((nimages*6, FEATURE_SIZE+1), dtype=np.float64)

    if aug is False:
        for i in tqdm(range(0, nimages)):
            sample, _, breed = dataReader.nextImage()
            x = image.img_to_array(sample)
            x = np.expand_dims(x, axis=0)
            x = preprocess_input(x)
            features[i, 0:FEATURE_SIZE] = feature_extractor.predict(x)
            features[i,FEATURE_SIZE] = int(breed)
    else:
        print("Generating augmented features")
        j = 0
        for i in tqdm(range(0, nimages)):
            samples, _, breed = dataReader.nextImage(aug=True)
            for sample in samples:
                x = sample.astype(np.float64)
                x = np.expand_dims(x, axis=0)
                x = preprocess_input(x)
                features[j, 0:FEATURE_SIZE] = feature_extractor.predict(x)
                features[j,FEATURE_SIZE] = int(breed)
                j += 1

    print(str(mode) + " feature generator finished")    
    print(features)
    np.save(mode + name + ".npy", features)

def perImageTest(featurename, mode, runCNN=False, bagging=False):
    dataReader.changeMode(mode)
    nimages = dataReader.read_counts[mode][1]

    classifier = loadclassifier(featurename, bagging=bagging)

    if runCNN is False:
        valfeatures = np.load(mode + featurename + ".npy")
        vlabels = valfeatures[:,FEATURE_SIZE].astype(int)
        vfeatures = valfeatures[:,0:FEATURE_SIZE]
    else:
        print("Loading InceptionV3...")
        feature_extractor = InceptionV3(input_shape=(299,299,3))
        feature_extractor.layers.pop()
        feature_extractor.outputs = [feature_extractor.layers[-1].output]
        feature_extractor.layers[-1].outbound_nodes = []
        #feature_extractor.summary()
        print("Loading done.")

    trues = 0
    for i in range(0, nimages):
        if runCNN is False:
            prediction = classifier.predict([vfeatures[i]])
            print("Correct label: " + str(vlabels[i]))
            print("Predicted label: " + str(prediction))
            if int(vlabels[i]) == int(prediction):
                trues += 1
        else:
            print("Runnig per image test...")
            sample, _, breed = dataReader.nextImage()
            x = image.img_to_array(sample)
            x = np.expand_dims(x, axis=0)
            x = preprocess_input(x)
            prediction = classifier.predict(feature_extractor.predict(x))
            if int(breed) == int(prediction[0]):
                trues += 1
            else:
                plt.imshow(sample)
                plt.title("Prediction: " + str(prediction[0]) + " breed:" + str(breed))
                plt.show()


        
    print("Accuracy: " + str(trues/nimages))


featurename = "featuresiv3"
if ATON:
    featurename = "aton-featuresiv3"
# TODO run by args
if len(argv) > 1:
    if argv[1] == "fit":
        trainclassifier(featurename, bagging=True)
    elif argv[1] == "fitsvm":
        trainclassifier(featurename)
    elif argv[1] == "fitsvmck":
        print("Tentando fitar com C=" + argv[2] + " e kernel " + argv[3])
        trainclassifier(featurename, ctest=int(argv[2]), kernel=argv[3])
    elif argv[1] == "fitcsvm":
        print("Tentando fitar com C=" + argv[2])
        trainclassifier(featurename, ctest=int(argv[2]))
    elif argv[1] == "fitsvmkernel":
        print("Tentando fitar kernel" + argv[2])
        trainclassifier(featurename, kernel=argv[2])
    elif argv[1] == "scores":
        validate(featurename)
    elif argv[1] == "predict":
        perImageTest(featurename, 'val')
    elif argv[1] == "predictshow":
        featurename += "aug"
        perImageTest(featurename, 'test', runCNN=True)
    elif argv[1] == "ctests":
        ps = []
        for c in [0.001, 0.01, 0.1, 1, 10, 100, 1000]:
            ps.append(Process(target=trainclassifier, args=(featurename, False, c, 'rbf')))
        for p in ps:
            p.start()
        for p in ps:
            p.join() 
    elif argv[1] == "auggen":
        featurename += "aug"
        generateFeatures(featurename, "train", aug=True)
        generateFeatures(featurename, "val", aug=True)
        generateFeatures(featurename, "test", aug=True)
    elif argv[1] == "augfit":
        featurename += "aug"
        trainclassifier(featurename)
    elif argv[1] == "augscore":
        featurename += "aug"
        validate(featurename)
else:
    generateFeatures(featurename, "train")
    generateFeatures(featurename, "val")
    generateFeatures(featurename, "test")
