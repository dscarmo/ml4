import sys
import keras
import tensorflow
from keras.applications import VGG16, inception_v3, resnet50, mobilenet
from keras import models
from keras import layers
from keras import optimizers
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Dropout, Flatten, Dense
from keras.optimizers import SGD
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Model
from keras.layers import AveragePooling2D, GlobalAveragePooling2D, Input
from sklearn.metrics import roc_auc_score
import numpy as np
from keras.applications.inception_v3 import preprocess_input
from keras.preprocessing.image import load_img 
import os
import sys
import glob
import argparse
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt



image_size = 299
train_dir = '../MO444_dogs/train'
validation_dir = '../MO444_dogs/val'
test_dir = '../MO444_dogs/test'

''' 
#Load the VGG model
vgg_model = vgg16.VGG16(weights='imagenet')
''' 
#Load the Inception_V3 model
inception_model = inception_v3.InceptionV3(weights='imagenet', include_top=False, input_shape=(image_size, image_size, 3))
''' 
#Load the ResNet50 model
resnet_model = resnet50.ResNet50(weights='imagenet')
 
#Load the MobileNet model
mobilenet_model = mobilenet.MobileNet(weights='imagenet')

#Load the VGG model
vgg_conv = VGG16(weights='imagenet', include_top=False, input_shape=(image_size, image_size, 3))
'''
# Freeze the layers except the last 4 layers [:-4]
for layer in inception_model.layers:
    layer.trainable = False
 
# Check the trainable status of the individual layers
for layer in inception_model.layers:
    print(layer, layer.trainable)

# Create the model
#model = models.Sequential()
 
# Add the vgg convolutional base model
#model.add(inception_model)
 
x = inception_model.output
# Re-add the layers here, with new weights.
x = GlobalAveragePooling2D(name='avg_pool')(x)
x = Dense(1024, activation='relu')(x)
x = Dropout(0.5)(x)
x = Dense(83, activation='softmax', name='predictions')(x)
model = Model(inputs=inception_model.inputs, outputs=x)

'''
# Add new layers
model.add(layers.Flatten())
model.add(layers.Dense(1024, activation='relu'))
model.add(layers.Dropout(0.5))
model.add(layers.Dense(83, activation='softmax'))
''' 
# Show a summary of the model. Check the number of trainable parameters
model.summary()

################################################
#Data generation and augumentation

train_datagen = ImageDataGenerator(
      rescale=1./255,
      preprocessing_function=preprocess_input
      #rotation_range=20,
      #brightness_range=(0.0,1.0),
      #horizontal_flip=True,
      #vertical_flip = True,
      #zoom_range=(1.0,1.2),
      #fill_mode='nearest'
      )

print(train_datagen)
 
validation_datagen = ImageDataGenerator(
    rescale=1./255,
    preprocessing_function=preprocess_input
    )
 
# Change the batchsize according to your system RAM
train_batchsize = 100
val_batchsize = 10
 
train_generator = train_datagen.flow_from_directory(
        train_dir,
        target_size=(image_size, image_size),
        batch_size=train_batchsize,
        class_mode='categorical')
 
validation_generator = validation_datagen.flow_from_directory(
        validation_dir,
        target_size=(image_size, image_size),
        batch_size=val_batchsize,
        class_mode='categorical',
        shuffle=False)

#############################################
#training and saving model

# Compile the model

model.compile(loss='categorical_crossentropy',
              optimizer=optimizers.RMSprop(lr=1e-4),
              metrics=['categorical_accuracy','fmeasure'])
# Train the model
history = model.fit_generator(
      train_generator,
      steps_per_epoch=train_generator.samples/train_generator.batch_size ,
      epochs=30,
      validation_data=validation_generator,
      validation_steps=validation_generator.samples/validation_generator.batch_size,
      verbose=1)
 
# Save the model
model.save('baseline_changeLastNoAug.h5')

##############################################
#performance

acc = history.history['categorical_accuracy']
val_acc = history.history['val_categorical_accuracy']
loss = history.history['loss']
val_loss = history.history['val_loss']
fmeasure = history.history['fmeasure']
val_fmeasure = history.history['val_fmeasure']
 
epochs = range(len(acc))
 
plt.plot(epochs, acc, 'b', label='Training acc')
plt.plot(epochs, val_acc, 'r', label='Validation acc')
plt.title('Training and validation accuracy')
plt.legend()
plt.savefig('accuracy_baseline_changeLastNoAug.png')
 
plt.figure()
 
plt.plot(epochs, loss, 'b', label='Training loss')
plt.plot(epochs, val_loss, 'r', label='Validation loss')
plt.title('Training and validation loss')
plt.legend()

plt.savefig('loss_baseline_changeLastNoAug.png')

plt.figure()
 
plt.plot(epochs, fmeasure, 'b', label='Training fmeasure')
plt.plot(epochs, val_fmeasure, 'r', label='Validation fmeasure')
plt.title('Training and validation fmeasure')
plt.legend()

plt.savefig('fmeasure_baseline_changeLastNoAug.png')
 
plt.show()

#############################################
#visualizing errors


# Create a generator for prediction
validation_generator = validation_datagen.flow_from_directory(
        validation_dir,
        target_size=(image_size, image_size),
        batch_size=val_batchsize,
        class_mode='categorical',
        shuffle=False)
 
# Get the filenames from the generator
fnames = validation_generator.filenames
 
# Get the ground truth from generator
ground_truth = validation_generator.classes
 
# Get the label to class mapping from the generator
label2index = validation_generator.class_indices
 
# Getting the mapping from class index to class label
idx2label = dict((v,k) for k,v in label2index.items())
 
# Get the predictions from the model using the generator
predictions = model.predict_generator(validation_generator, steps=validation_generator.samples/validation_generator.batch_size,verbose=1)
predicted_classes = np.argmax(predictions,axis=1)
 
errors = np.where(predicted_classes != ground_truth)[0]
print("No of errors = {}/{}".format(len(errors),validation_generator.samples))
 
# Show the errors
for i in range(len(errors)):
    pred_class = np.argmax(predictions[errors[i]])
    pred_label = idx2label[pred_class]
     
    title = 'Original label:{}, Prediction :{}, confidence : {:.3f}'.format(
        fnames[errors[i]].split('/')[0],
        pred_label,
        predictions[errors[i]][pred_class])
     
    original = load_img('{}/{}'.format(validation_dir,fnames[errors[i]]))
    plt.figure(figsize=[7,7])
    plt.axis('off')
    plt.title(title)
    plt.savefig(str(i)+'error_baseline_changeLastNoAug.png')
    plt.imshow(original)
    plt.show()

