from matplotlib import pyplot as plt
import numpy as np

c = [0.001, 0.01, 0.1, 1, 10, 100, 1000]

trainacc = [0.9243373493975904, 0.9243373493975904, 0.9255421686746987, 0.9479518072289157,
            0.9822891566265061, 0.9995180722891567, 0.9995180722891567]
valacc = [0.9237794752573896, 0.9237794752573896, 0.9251079375622717, 0.9382265028229824,
          0.9390567917635337, 0.9310860179342411, 0.9309199601461309]
#testacc = [0.9396678966789668, 0.9396678966789668, 0.9404059040590406, 0.9492619926199262,
 #          0.9490774907749078, 0.935239852398524, 0.9341328413284132]
plt.plot(np.log(c), np.asarray(trainacc) * 100, 'b*-', label='Train')
plt.legend()
plt.plot(np.log(c), np.asarray(valacc) * 100, 'r*-', label='Validation')
plt.legend()
#plt.plot(np.log(c), np.asarray(testacc) * 100, 'g*-', label='Test')
#plt.legend()
plt.xlabel("log10(C)")
plt.ylabel("Normalized accuracy (%)")
ax = plt.gca()
ax.set_ylim([90, 101])
plt.show()


plt.figure()

f1val = [0.9202795695478424, 0.9202795695478424, 0.9217824286931199, 0.93465593487479,
        0.9355661618196662, 0.9269676186767694, 0.9268748770177798]
#f1test = [0.9339951014937619, 0.9339951014937619, 0.9349143796434692, 0.9447134784681799,
#         0.9441045424397868, 0.9316469824838527, 0.9305676366159007]

plt.plot(np.log(c), f1val, 'r*-', label='Validation')
plt.legend()
#plt.plot(np.log(c), f1test, 'g*-', label='Test')
#plt.legend()
plt.xlabel("log10(C)")
plt.ylabel("F1 score ")
ax = plt.gca()
ax.set_ylim([0.9, 1.01])
plt.show()
