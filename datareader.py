'''
Defines and tests the DataReader class
'''
import os
from keras.preprocessing import image
from matplotlib import pyplot as plt
from augmentation import augmentate


FINISHED = -1

class DataReader:
    '''
    This class does all the stuff to read files from directories
    No order is guaranteed
    '''
    def __init__(self, datatopdir, mode = "train"):
        '''
        Initializes class to hold all directory data info
        such as folder names and names of files inside those 
        folders
        '''
        self.datatopdir = datatopdir
        self.dirs = {}
        self.dir_names = []
        self.dir_images = {}
        self.read_counts = {}

        for root, dirs, _ in os.walk(self.datatopdir, topdown=False):
            for name in dirs:
                path = os.path.join(root, name)
                name = os.path.basename(path)
                self.dirs[name] = path
                self.dir_names.append(name)
                self.dir_images[name] = os.listdir(path)

        print("Detected the following directories in data folder:")
        print(self.dirs)
        print("Operation modes will be set to:")
        print(self.dir_names)
        for dir_, images in self.dir_images.items():
            print(dir_ + " has " + str(len(images)) + " images.")
            self.read_counts[dir_] = [0, len(images)]
        try:
            self.assertMode(mode)
        except ValueError as e:
            print("Error reading data: " + str(e) + " datareader is not usable.")
        self.mode = mode

    def assertMode(self, mode):
        if mode not in self.read_counts:
            raise ValueError("Mode should be 'train' 'test' or 'val'.")
        
    def changeMode(self, mode):
        self.assertMode(mode)
        self.mode = mode
        print("reader mode is now " + str(mode))

    def nextIndex(self):
        '''
        Chekcs if user went thorugh all pictures
        '''
        count_tuple = self.read_counts[self.mode]
        index = count_tuple[0]
        count_tuple[0] += 1

        if count_tuple[0] > count_tuple[1]:
            print("Got all pictures from " + self.mode)
            return FINISHED
        else:
            self.read_counts[self.mode] = count_tuple
        
        return index

    def nextImage(self, mode=None, size=(299, 299), aug=False):
        '''
        Gets the next image and its label on the specified mode
        If not defined, use the defautl defined on construction
        '''
        if mode is not None:
            self.changeMode(mode)
        
        i = self.nextIndex()
        if i == FINISHED:
            return FINISHED
        else:
            img_name = self.dir_images[self.mode][i]
            path = self.dirs[self.mode] + "/" + img_name
            if aug is False:
                ret = [image.load_img(path, target_size=size), img_name[2:], img_name[0:2]]
            else:
                [img, label, breed] = image.load_img(path, target_size=size), img_name[2:], img_name[0:2]
                ret = [augmentate(image.img_to_array(img), v=True), label, breed]

            return ret


def dataReaderUnitTest():
    dataReader = DataReader("/home/diedre/bigdata/dogs", mode='train')
    image = not FINISHED
    while image is not FINISHED:
        image, label, breed = dataReader.nextImage()
        plt.imshow(image)
        plt.title(str(dataReader.mode) + "- Breed: " + breed + " Label:" + label)
        plt.draw()
        if plt.waitforbuttonpress(timeout=1) is None:
            plt.close()
        else:
            quit()

# Debug section
#dataReaderUnitTest()



