'''
Data augmentation functions
From one image get lots of imgs =D
'''
from random import randint, choice
from skimage.transform import rotate
from matplotlib import pyplot as plt
from scipy.ndimage.interpolation import zoom
import numpy as np
import cv2


def Print(msg, verbose):
    if verbose is True:
        print(msg)

def cv2_clipped_zoom(img, zoom_factor):
    """
    Center zoom in/out of the given image and returning an enlarged/shrinked view of 
    the image without changing dimensions
    Args:
        img : Image array
        zoom_factor : amount of zoom as a ratio (0 to Inf)
    """
    height, width = img.shape[:2] # It's also the final desired shape
    new_height, new_width = int(height * zoom_factor), int(width * zoom_factor)

    ### Crop only the part that will remain in the result (more efficient)
    # Centered bbox of the final desired size in resized (larger/smaller) image coordinates
    y1, x1 = max(0, new_height - height) // 2, max(0, new_width - width) // 2
    y2, x2 = y1 + height, x1 + width
    bbox = np.array([y1,x1,y2,x2])
    # Map back to original image coordinates
    bbox = (bbox / zoom_factor).astype(np.int)
    y1, x1, y2, x2 = bbox
    cropped_img = img[y1:y2, x1:x2]

    # Handle padding when downscaling
    resize_height, resize_width = min(new_height, height), min(new_width, width)
    pad_height1, pad_width1 = (height - resize_height) // 2, (width - resize_width) //2
    pad_height2, pad_width2 = (height - resize_height) - pad_height1, (width - resize_width) - pad_width1
    pad_spec = [(pad_height1, pad_height2), (pad_width1, pad_width2)] + [(0,0)] * (img.ndim - 2)

    result = cv2.resize(cropped_img, (resize_width, resize_height))
    result = np.pad(result, pad_spec, mode='constant')
    assert result.shape[0] == height and result.shape[1] == width
    return result

def augmentate(original_img, v=True):
    '''
    From an original img return it and:
    Random rotation from -20 to +20
    Random brightness decrease (multiply by factor) from 0 to 0.9
    Horizontal flip
    Vertical flip
    Random zoom from 1.1 to 1.2
    '''
    rint = randint(1, 10)
    neg = choice((-1, 1))
    rnumber = neg*rint
    #Print("Random number: " + str(rnumber), v)
    
    rotated = (rotate(original_img.astype(np.uint8), 2*rnumber, order=3, mode='edge')*255).astype(np.uint8)
    dark = (original_img*(0.3 + 0.6/rint)).astype(np.uint8)
    horflip = np.fliplr(original_img).astype(np.uint8)
    verflip = np.flipud(original_img).astype(np.uint8)
    zoomed = cv2_clipped_zoom(original_img, 1 + rint/10).astype(np.uint8)

    if v is True:
        cv2.imshow("original", cv2.cvtColor(original_img.astype(np.uint8), cv2.COLOR_RGB2BGR))
        cv2.imshow("rotated", cv2.cvtColor(rotated, cv2.COLOR_RGB2BGR))
        cv2.imshow("dark", cv2.cvtColor(dark, cv2.COLOR_RGB2BGR))
        cv2.imshow("horflip", cv2.cvtColor(horflip, cv2.COLOR_RGB2BGR))
        cv2.imshow("verflip", cv2.cvtColor(verflip, cv2.COLOR_RGB2BGR))
        cv2.imshow("zoomed", cv2.cvtColor(zoomed, cv2.COLOR_RGB2BGR))
        if cv2.waitKey(1) == 27:
            quit()
    return [original_img, rotated, dark, horflip, verflip, zoomed]

    
def augtest():
    cap = cv2.VideoCapture(0)
    while True:
        ret, frame = cap.read()
        if ret is True:
            augmentate(frame)

#augtest()